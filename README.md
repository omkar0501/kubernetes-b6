# Kubernetes

## What is Kubernetes?
-Kubernetes is an open-source container orchestration system for automating software deployment, scaling, and management. Originally designed by Google, the project is now maintained by the Cloud Native Computing Foundation.

## Why we use kubernetes?
Kubernetes services provide load balancing and simplify container management on multiple hosts. They make it easy for an enterprise's apps to have greater scalability and be flexible, portable and more productive. In fact, Kubernetes is the fastest growing project in the history of open-source software, after Linux.

## Features of kubernetes?
-Kubernetes has many features that help orchestrate containers across multiple hosts, automate the management of K8s clusters, and maximize resource usage through better utilization of infrastructure. Important features include:
-Auto-scaling. Automatically scale containerized applications and their resources up or down based on usage
-Lifecycle management. Automate deployments and updates with the ability to:
      Rollback to previous versions
      Pause and continue a deployment
-Declarative model. Declare the desired state, and K8s works in the background to maintain that state and recover from any failures
-Resilience and self-healing. Auto placement, auto restart, auto replication and auto scaling provide application self-healing
-Persistent storage. Ability to mount and add storage dynamically
-Load balancing. Kubernetes supports a variety of internal and external load balancing options to address diverse needs.

## Kubernetes Architecture?
The Kubernetes architecture is designed to run containerized applications. A Kubernetes cluster consists of at least one control plane and at least one worker node (typically a physical or virtual server). The control plane has two main responsibilities. It exposes the Kubernetes API through the API server and manages the nodes that make up the cluster. The control plane makes decisions about cluster management and detects and responds to cluster events.
The smallest unit of execution for an application running in Kubernetes is the Kubernetes Pod, which consists of one or more containers. Kubernetes Pods run on worker nodes.

## Pod Lifecycle
Pods follow a defined lifecycle, starting in the Pending phase, moving through Running if at least one of its primary containers starts OK, and then through either the Succeeded or Failed phases depending on whether any container in the Pod terminated in failure.
*lifecycle consists of several phases:-
-Pending: The Pod has been accepted by the Kubernetes system, but one or more of the Container images has not been created. This includes time before being scheduled as well as time spent downloading images over the network, which could take a while.

-Running: The Pod has been bound to a node, and all of the Containers have been created. At least one Container is still running, or is in the process of starting or restarting.

-Succeeded: All Containers in the Pod have terminated in success, and will not be restarted.

-Failed: All Containers in the Pod have terminated, and at least one Container has terminated in failure. That is, the Container either exited with non-zero status or was terminated by the system.

-Unknown: For some reason the state of the Pod could not be obtained, typically due to an error in communicating with the host of the Pod.

## Objects:
- Pod - Smallest unit of kubernetes. Its just wrapping around the container
- Service - Expose application 
    - ClusterIP - Expose application inside the cluster
    - NodePort - Expose application outside the cluster
    - LoadBalancer - Expose application outside the cluster 
- Namespace -  To segregate the resources / k8s objects